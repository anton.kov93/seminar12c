﻿#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "Voice" << std::endl;;
    }
};


class Dog : public Animal 
{
public:
    void Voice() override
    {
        std::cout << "Woof!" << std::endl;;
    }
};


class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Miau!" << std::endl;;
    }
};


class Sheep : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Beee!" << std::endl;;
    }
};


int main()
{
    const int arraySize = 3;
    Animal* animals[arraySize];

    // Создание объектов и добавление в массив
    animals[0] = new Dog();
    animals[1] = new Cat();
    animals[2] = new Sheep();

    // Вызов метода Voice() для каждого элемента массива
    for (int i = 0; i < arraySize; ++i) 
    {
        animals[i]->Voice();
    }

    // Освобождение памяти
    for (int i = 0; i < arraySize; ++i)
    {
        delete animals[i];
    }

    return 0;
}

